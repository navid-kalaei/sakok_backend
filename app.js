var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var imageRouter = require('./routes/image');
var categoryRouter = require('./routes/category');
var tagRouter = require('./routes/tag');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/image', imageRouter);
app.use('/category', categoryRouter);
app.use('/tag', tagRouter);

module.exports = app;
