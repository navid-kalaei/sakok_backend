var express = require('express');
var router = express.Router();
var Image = require('../models/Image');
var path = require('path');
var multer = require('multer');


var upload = multer({
	fileFilter: function (req, file, cb) {
		if (file.mimetype.split('/')[0] === 'image') {
			return cb(null, true)
		}
		cb(null, false)
	},
});

router.get('/', function(req, res, next) {
	Image.find().populate('category').exec()
		.then(images => {
			const imagesObject = images.reduce((imageAccumulator, currentImage) =>{
				imageAccumulator[currentImage._id] = currentImage;
				return imageAccumulator
			}, {})
			res.json(imagesObject)
		})
		.catch(err => res.send(err));
});

router.post('/', upload.single('image'),function (req, res, next) {
	var {title, description, tags, category} = req.body;
	var image = req.file;

	var newImage = new Image();
	newImage.title = title;
	newImage.tags = tags;
	newImage.description = description;
	newImage.category = category;
	newImage.image.data = image.buffer;
	newImage.image.mimetype = image.mimetype;
	newImage.image.size = image.size;
	newImage.save((err, image) => {
		Image.findOne({_id: image._id}).populate('category').exec().then(savedImage => res.send(savedImage))
	});
});

module.exports = router;
