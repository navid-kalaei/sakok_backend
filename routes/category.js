var express = require('express');
var router = express.Router();
var Category = require('../models/Category');

router.get('/', function(req, res, next) {
	Category.find().exec().then(categories => res.json(categories)).catch(err => res.send(err));
});

router.post('/', function (req, res, next) {
	var {value, label} = req.body;

	var newCategory = new Category();
	newCategory.value = value;
	newCategory.label = label || value;
	// TODO: catch errors
	newCategory.save();

	res.send(newCategory);
});

module.exports = router;
