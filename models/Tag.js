var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var datastores = require('../config/datastores');
var tagDatastore = datastores.tag || datastores.default;
var timestampPlugin = require('./plugins/timestamp');

mongoose.connect(tagDatastore.url, { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: tag'));

var tagSchema = new Schema({
	name: String,
});

tagSchema.plugin(timestampPlugin);

var Tag = mongoose.model('Tag', tagSchema);

module.exports = Tag;
