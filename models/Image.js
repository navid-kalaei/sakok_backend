var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var datastores = require('../config/datastores');
var imageDatastore = datastores.image || datastores.default;
var timestampPlugin = require('./plugins/timestamp');

mongoose.connect(imageDatastore.url, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: image'));

var ImageSchema = new Schema({
	title: String,
	description: String,
	tags: [String],
	category: {type: Schema.Types.ObjectId, ref: 'Category'},
	image: {
		data: Buffer,
		mimetype: String,
		size: Number,
	},
});

ImageSchema.plugin(timestampPlugin);

var Image = mongoose.model('Image', ImageSchema);

module.exports = Image;
