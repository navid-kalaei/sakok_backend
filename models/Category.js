var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var datastores = require('../config/datastores');
var categoryDatastore = datastores.category || datastores.default;
var timestampPlugin = require('./plugins/timestamp');

mongoose.connect(categoryDatastore.url, { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: category'));

var CategorySchema = new Schema({
	value: String,
	label: String,
});

CategorySchema.plugin(timestampPlugin);

var Category = mongoose.model('Category', CategorySchema);

module.exports = Category;
